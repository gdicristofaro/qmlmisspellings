import Qt.labs.platform 1.0
import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2
import QtCharts 2.0

ApplicationWindow {
    property bool dictLoaded: false
    property bool showChart: false
    property var wordList: [] //["hi", "hello", "no"]
    property var numTimesList: []//[1,2,50]
    property var maxYVal: 0

    property bool showDictDialog: false
    property bool showTextDialog: false

    property string dictionaryName: ""
    property string textName: ""

    visible: true
    width: 640
    height: 480
    title: qsTr("Misspellings")

    FileDialog {
        id: dictDialog
        visible: showDictDialog
        modality: Qt.NonModal
        title: "Choose the Dictionary"
        selectExisting: true
        selectMultiple: false
        selectFolder: false
        nameFilters: [ "All files (*)" ]
        selectedNameFilter: "All files (*)"
        sidebarVisible: true
        folder: StandardPaths.standardLocations(StandardPaths.DocumentsLocation)[0]
        onAccepted: {
            handler.loadDict(fileUrl);
            showDictDialog = false;
        }
        onRejected: {
            showDictDialog = false;
        }
    }

    FileDialog {
        id: textDialog
        visible: showTextDialog
        modality: Qt.NonModal
        title: "Choose the Text File"
        selectExisting: true
        selectMultiple: false
        selectFolder: false
        nameFilters: [ "All files (*)" ]
        selectedNameFilter: "All files (*)"
        sidebarVisible: true
        folder: StandardPaths.standardLocations(StandardPaths.DocumentsLocation)[0]
        onAccepted: {
            handler.loadText(fileUrl);
            showTextDialog = false;
        }
        onRejected: {
            showTextDialog = false;
        }
    }


    ColumnLayout {
        id: columnLayout
        anchors.right: parent.right
        anchors.rightMargin: 8
        anchors.left: parent.left
        anchors.leftMargin: 8
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 8
        anchors.top: parent.top
        anchors.topMargin: 8

        RowLayout {
            id: rowLayout
            height: 80
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            spacing: 10

            ColumnLayout {
                id: columnLayout1
                anchors.top: parent.top
                anchors.topMargin: 0
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                spacing: 10
                Layout.fillWidth: true
                Layout.fillHeight: true

                Button {
                    id: loaddict
                    text: qsTr("Load Dictionary File...")
                    onClicked: { showDictDialog = true; }
                }

                Text {
                    id: loaddictloc
                    text: qsTr(dictionaryName)
                    font.pixelSize: 12
                }
            }

            ColumnLayout {
                id: columnLayout2
                anchors.top: parent.top
                anchors.topMargin: 0
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                spacing: 10
                Layout.fillWidth: true
                Layout.fillHeight: true

                Button {
                    id: loadtext
                    enabled: dictLoaded
                    text: qsTr("Load Text File...")
                    onClicked: { showTextDialog = true; }
                }

                Text {
                    id: loadtextloc
                    text: qsTr(textName)
                    font.pixelSize: 12
                }
            }
        }



        Item {
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.top: parent.top
            anchors.topMargin: 88

            ChartView {
                id: misspellingschart
                title: "Misspellings"
                anchors.fill: parent
                antialiasing: true
                visible: showChart

                BarSeries {
                    id: barchart
                    axisX: BarCategoryAxis {
                        categories: wordList
                    }
                    axisY: ValueAxis {
                        min: 0
                        max: maxYVal
                    }
                    BarSet {
                        id: barset
                        label: "Most Common Misspelled Words"
                        values: numTimesList
                    }
                }
            }
        }
    }
}

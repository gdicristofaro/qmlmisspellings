#include "spellcheckhandler.h"
#include <iostream>
#include <QString>
#include <QVariant>
#include <QStringList>
#include <QVariantList>
#include <QObject>
#include <QtWidgets>
#include <QtCharts/QChart>
#include <QtCharts/QChartView>
#include <QTextStream>
#include <QIODevice>
#include <QMessageBox>
#include <functional>
#include <regex>
#include <boost/algorithm/string.hpp>

#include <tuple>
#include <unordered_set>
#include <map>
#include <string>
#include <algorithm>
#include <vector>
#include <iostream>

using std::unordered_set;
using std::map;
using std::string;
using std::function;
using std::tuple;
using std::tie;
using std::regex;
using std::smatch;
using std::vector;
using std::cout;





#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>


// hashSize determines the size of the hashtable
// this hash function inspired by the string hashcode function in jdk takes advantage
// of sizes typically being multiples of 32 I believe.
// hash size should be a multiple of 32 for that reason.
#define HASH_SIZE 32768

//linked list adapted from http://www.cprogramming.com/tutorial/c/lesson15.html
struct node {
    char* string;
    struct node* next;
} __attribute__((__packed__));

//create node holder for reading in dictionary
struct node* arr = NULL;

//buffer for strings
char* buffer = NULL;


bool stringCmp(char* dictionaryWord, const char* string, int stringlength)
{
    //maybe there are some extra characters in dictionaryWord...that would be a problem
    if (dictionaryWord[stringlength] != '\0')
        return false;

    //check all letters that are consistent to dictionaryWord and string (that exist in stringlength
    for (int i = 0; i < stringlength; i++)
    {
        // the goal of this operation is to compare the dictionary char and string char
        // the ^ operator should take care of equality concerning the necessary bits by setting them to 0 if equal
        // the 0x5F should deal with the case issue (i.e. a and A are both seen as 65 with this)
        // however, it should keep the apostrophe lower so that '(39  to 7) does not equal g (103 to 71 and not 7 if
        // we took out the 64)
        // if it isn't 0, then they aren't equal
        //used as source: http://www.cs.umd.edu/class/sum2003/cmsc311/Notes/BitOp/bitwise.html
        if ((string[i] ^ dictionaryWord[i]) & 0x5F)
            return false;
    }

    //we made it through the tests and we are all set
    return true;
}


/**
 * Returns true if word is in dictionary else false.
 */
bool check(const char* word)
{
    //figure out string length and hash (at the same time)
    int length = 0;
    unsigned int curHash = 0;

    while(word[length] != '\0')
    {
        // adapted from http://hg.openjdk.java.net/jdk7/build-gate/jdk/file/tip/src/share/classes/java/lang/String.java (the hashCode part)
        // adjust for upper/lowercase using & 0x1F - this should only take the bits under 32 which is the only piece we need to determine the letter
        // this will mean that ' will have the same value as a letter, but I think it is a small issue given the efficiency
        curHash = curHash * 31 + (word[length] & 0x1F);
        length++;
    }

    //do bounds checking on the hash (make sure it isn't larger than hashSize and consequently, the arr)
    int thehash = (int) (curHash % HASH_SIZE);

    //if hash doesn't exist, then it isn't a word
    if (arr[thehash].string == NULL)
        return false;

    //if hash does exist, then check it to see if it is ok
    if (stringCmp(arr[thehash].string, word, length))
        return true;

    //so the first node didn't work, let's try another one in the linked list
    struct node current = arr[thehash];
    while ( current.next != NULL)
    {
        current = *(current.next);
        if (stringCmp(current.string, word, length))
            return true;
    }

    //that failed too, so it isn't in dictionary
    return false;
}


// TODO REMOVE: for diagnostic purposes only
/*
int totalwords = 0;
int duplicatehashes = 0;
int maxdepth = 0;

void printStats() {
    printf("\n\nDIAGNOSTICS: \n\ttotal words loaded: %d\n\tduplicatehashes: %d\n\tmax depth: %d\n\n", totalwords, duplicatehashes, maxdepth);
}
*/

/**
 * Loads dictionary into memory.  Returns true if successful else false.
 */
bool load(const char* dictionary)
{
    arr = (node*)malloc(sizeof(struct node) * HASH_SIZE);
    if (arr == NULL)
        return false;

    //set it to null so we can be clear what is going on inside
    //pointers to pointers credited here: http://www.eskimo.com/~scs/cclass/int/sx8.html
    for (int i = 0; i < HASH_SIZE; i++)
        arr[i].string = NULL;

    FILE *input = fopen( dictionary, "r" );
    if (input == NULL)
        return false;

    //this will handle the hash of each word
    unsigned int currentHash = 0;

    //fseek to determine length - http://www.linuxquestions.org/questions/programming-9/how-to-get-size-of-file-in-c-183360/
    fseek(input, 0, SEEK_END);

    //set the buffer up - I am malloc'ing the size of the dictionary file plus a byte
    //I am going to put all of the words into buffer and then chop up buffer
    buffer = (char*)malloc(ftell(input) + 1);
    if (buffer == NULL)
        return false;

    //go back to the right place in the file
    fseek(input, 0, SEEK_SET);

    // curLength will tell us where the character should go
    // curStringStart will tell us where the next string will start
    int curLength = 0;
    int curStringStart = 0;

    //read in input
    while (!feof(input))
    {
        char nextChar = fgetc(input);


        if (nextChar == '\n')
        {
            //TODO remove below
            //totalwords++;

            //end that piece of the string
            buffer[curLength] = '\0';

            //get hash
            int hash = (int) (currentHash % HASH_SIZE);

            //put word in the hash if there isn't one there already
            if (arr[hash].string == NULL) {
                //set string of current node to the piece of buffer that we want
                arr[hash].string = &buffer[curStringStart];
                arr[hash].next = NULL;
            }
            //if there is a word in the hash already, append to linked list
            else
            {
                //TODO remove 2 below
                //duplicatehashes++;
                //int traversecounter = 2;

                struct node* traverse = &arr[hash];
                while (traverse->next != NULL)
                {
                    //TODO remove below
                    //traversecounter++;

                    traverse = traverse->next;
                }

                //TODO remove below
                //if (traversecounter > maxdepth) maxdepth = traversecounter;

                traverse->next = (node*)malloc(sizeof(struct node));
                    //set this node to the string
                    traverse->next->string = &buffer[curStringStart];
                    traverse->next->next = NULL;
            }

            //increment where we are, reset
            curLength++;
            currentHash = 0;

            //start of the next string will be the current length after we increment
            curStringStart = curLength;
            continue;
        }

        //grab the hash
        //hash function explained in line 81
        currentHash = currentHash * 31 + (nextChar & 0x1F);
        buffer[curLength] = nextChar;
        curLength++;
    }

    //close the file
    fclose(input);

    //TODO remove below
    //printStats();

    return true;
}

//cleans out a node in linked list
void cleanNode(struct node* thenode)
{
    //recurse if there is more
    if (thenode->next != NULL)
        cleanNode(thenode->next);

    //free the node
    free(thenode);
    return;
}


/**
 * Unloads dictionary from memory.  Returns true if successful else false.
 */
bool unload(void)
{
    // if not loaded, don't unload
    if (arr == NULL)
        return true;

    free(buffer);

    //for all nodes in array, erase...
    for (int i = 0; i < HASH_SIZE; i++)
    {
        //clean nodes if not null
        if (arr[i].next != NULL)
            cleanNode(arr[i].next);
    }
    //free the node holder
    free(arr);

    arr = NULL;
    buffer = NULL;

    return true;
}


const int LENGTH = 50;

// taken from CS50's speller homework code
bool checkText(const char* textPath, map<string,int>& misspelledItems)
{
    FILE *fp = fopen( textPath, "r" );
    if (fp == NULL)
        return false;

    // prepare to spell-check
     int index = 0;

     // english words aren't longer than 50 characters
     char word[LENGTH + 1];

     // spell-check each word in text
     for (int c = fgetc(fp); c != EOF; c = fgetc(fp))
     {
         // allow only alphabetical characters and apostrophes
         if (isalpha(c) || (c == '\'' && index > 0))
         {
             // append character to word
             word[index] = c;
             index++;

             // ignore alphabetical strings too long to be words
             if (index > LENGTH)
             {
                 // consume remainder of alphabetical string
                 while ((c = fgetc(fp)) != EOF && isalpha(c));

                 // prepare for new word
                 index = 0;
             }
         }

         // ignore words with numbers (like MS Word can)
         else if (isdigit(c))
         {
             // consume remainder of alphanumeric string
             while ((c = fgetc(fp)) != EOF && isalnum(c));

             // prepare for new word
             index = 0;
         }

         // we must have found a whole word
         else if (index > 0)
         {
             // terminate current word
             word[index] = '\0';

             bool misspelled = !check(word);

             // print word if misspelled
             if (misspelled)
             {
                 if (misspelledItems.find(word) == misspelledItems.end())
                     misspelledItems[word] = 1;
                 else
                     misspelledItems[word] = misspelledItems[word] + 1;
             }

             // prepare for next word
             index = 0;
         }
     }

     return true;
}


vector<tuple<string,int>> getOrderedmisspelled(map<string,int>& misspelled, vector<tuple<string,int>>& ordered, int maxSize) {
    for(auto const &entr : misspelled) {
        tuple<string,int> item(entr.first, entr.second);

        // insert index
        int i = ordered.size() - 1;
        for (; i >= 0; i--)
        {
            auto otherTup = ordered.at(i);
            // if number of misspellings is less than current element
            if (std::get<1>(item) < std::get<1>(otherTup) ||
                    // or they are equal and this item should come later alphabetically
                    ((std::get<1>(item) == std::get<1>(otherTup)) &&
                        (std::get<0>(item).compare(std::get<0>(otherTup)) >= 0)))
                            // we have the index to insert after
                            break;
        }

        // we need to insert after
        i++;

        // no need to insert if it doesn't belong
        if (i >= maxSize)
            continue;

        ordered.insert(ordered.begin() + i, item);

        if (ordered.size() > maxSize)
            ordered.pop_back();
    }

    return ordered;
}





spellcheckhandler::spellcheckhandler(QObject *parent) : QObject(parent) {}

void spellcheckhandler::loadDict(QString url) {
    QUrl theUrl(url);
    auto path = theUrl.path().toLatin1().data();

    // unload dictionary
    unload();
    load(path);

    auto p = this->parent();
    p->setProperty("dictLoaded", "true");
    p->setProperty("dictionaryName",theUrl.fileName());


    // for debug
    /*
    for (int hash = 0; hash < HASH_SIZE; hash++) {
        if (arr[hash].string == NULL)
            continue;

        cout << arr[hash].string << "\n";

        auto item = arr[hash];
        while (item.next != NULL) {
            item = *(item.next);
            cout << item.string << "\n";
        }
    }
    */
}

void spellcheckhandler::loadText(QString url) {
    QUrl theUrl(url);
    auto path = theUrl.path().toLatin1().data();

    map<string,int> misspelled;

    // if check text failed, return
    if (!checkText(path, misspelled))
        return;

    vector<tuple<string, int>> ordered;
    getOrderedmisspelled(misspelled, ordered, 10);

    QVariantList numlst;
    QStringList wordlst;
    int max = 0;
    for (tuple<string, int> tup : ordered) {
        string word = std::get<0>(tup);
        wordlst += QString::fromStdString(word);
        int curNum = std::get<1>(tup);
        numlst += curNum;
        max = std::max(max, curNum);
    }

    auto p = this->parent();
    p->setProperty("numTimesList",numlst);
    p->setProperty("wordList", wordlst);
    p->setProperty("showChart", "true");
    p->setProperty("maxYVal", max);
    p->setProperty("textName",theUrl.fileName());
}

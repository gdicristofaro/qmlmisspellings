import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtCharts 2.0

Item {
    id: item1
    visible: true

    ColumnLayout {
        id: columnLayout
        anchors.right: parent.right
        anchors.rightMargin: 8
        anchors.left: parent.left
        anchors.leftMargin: 8
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 8
        anchors.top: parent.top
        anchors.topMargin: 8

        RowLayout {
            id: rowLayout
            height: 200
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            spacing: 10

            ColumnLayout {
                id: columnLayout1
                anchors.top: parent.top
                anchors.topMargin: 0
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                spacing: 10
                Layout.fillWidth: true
                Layout.fillHeight: true

                Button {
                    id: loaddict
                    text: qsTr("Load Dictionary File...")
                }

                Text {
                    id: loaddictloc
                    text: qsTr("")
                    font.pixelSize: 12
                }
            }

            ColumnLayout {
                id: columnLayout2
                anchors.top: parent.top
                anchors.topMargin: 0
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                spacing: 10
                Layout.fillWidth: true
                Layout.fillHeight: true

                Button {
                    id: loadtext
                    text: qsTr("Load Text File...")
                }

                Text {
                    id: loadtextloc
                    text: qsTr("")
                    font.pixelSize: 12
                }
            }
        }

        ChartView {
            title: "Misspellings"
            anchors.fill: parent
            antialiasing: true
            height: 200
            width: 200
            visible: true

            BarSeries {
                id: barchart
                axisX: BarCategoryAxis {
                    categories: ["2007", "2008", "2009", "2010", "2011", "2012"]
                }
                BarSet {
                    label: "Bob"
                    values: [2, 2, 3, 4, 5, 6]
                }
                BarSet {
                    label: "Susan"
                    values: [5, 1, 2, 4, 1, 7]
                }
                BarSet {
                    label: "James"
                    values: [3, 5, 8, 13, 5, 8]
                }
            }
        }
    }
}

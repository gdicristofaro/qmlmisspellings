#ifndef SPELLCHECKHANDLER_H
#define SPELLCHECKHANDLER_H

#include <QObject>
#include <unordered_set>
#include <string>
using std::unordered_set;
using std::string;

class spellcheckhandler : public QObject
{
    Q_OBJECT
public:
    explicit spellcheckhandler(QObject *parent = nullptr);


signals:

public slots:
    void loadDict(QString path);
    void loadText(QString path);

private:
    unordered_set<string> dictionary;
};

#endif // SPELLCHECKHANDLER_H

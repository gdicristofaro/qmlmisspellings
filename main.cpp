#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <spellcheckhandler.h>



int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    spellcheckhandler handler(engine.rootObjects().first());
    engine.rootContext()->setContextProperty("handler", &handler);

    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
